import React, { Component } from 'react';
import axios from 'axios';

class App extends Component {
  state = {
    formFields:{
      to: '',
      subject: '',
      message: ''
    }
  }


  getFormValues = (event) => {
    const formFields = {...this.state.formFields};
    
    if(event.target.type === 'file'){
      formFields[event.target.name] = event.target.files[0];   
    }else{
      formFields[event.target.name] = event.target.value;
    }
    
    this.setState({formFields});
  }


  sendEmail = () => {
    const formData = new FormData();
    for(let obj in this.state.formFields){
      formData.append(obj, this.state.formFields[obj]);
    }
    
    axios.post('/upload', formData).then((response) => {
      console.log(response); // do something with the response
    });
  }


  render() {
    return (
      <div className="App">
          <input type="text" name="to" placeholder="to" onKeyUp={this.getFormValues} />
          <input type="text" name="subject" placeholder="subject" onKeyUp={this.getFormValues} />
          <input type="text" name="message" placeholder="message" onKeyUp={this.getFormValues} />
          <input type="file" name="image" onChange={this.getFormValues} />
          <button type="submit" onClick={this.sendEmail}>Submit</button>
      </div>
    );
  }
}

export default App;

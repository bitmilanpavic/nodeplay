const express  = require('express');
const app = express();
const nodemailer = require('nodemailer');
const transporter = require('./mail.js');
const bodyParser = require('body-parser');
const multer  = require('multer')
const path = require('path');


// Middleware
app.use(express.static(path.join(__dirname, 'client/build')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
});
let upload = multer({ storage: storage });
// npm install --only=dev && npm install &&

// Routes
app.post('/upload', upload.single('image'), (req, res)=>{
    const to = req.body.to,
    subject = req.body.subject,
    message = req.body.message;
    console.log(req.file);
    
    // Setup email data
    const mailOptions = {
        from: '"Fred Foo 👻" <milanpavice@gmail.com>', 
        to: to, 
        subject: subject, 
        attachments: [{
            filename: req.file.originalname,
            path: req.file.path
        }],
        html: `<b>${message}</b>`
    };
    
    //Send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            res.json({emailSend: false, error:error, info: info});
        }
        res.json({emailSend: true});
    });
});

// Listen
const port = process.env.PORT || 3001;
app.listen(port, () => {
    console.log('Listen on port:', port);
});